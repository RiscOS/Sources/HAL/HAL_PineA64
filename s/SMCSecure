; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;

; Secure mode monitor required for turning on L2 cache in respose to SMC #0
; service call
;
;
; no GETs needed.. we are included using a GET
;
;        AREA    |Asm$$Code|, CODE, READONLY, PIC
;        EXPORT SecureInit

        ALIGN   32
SecureBase
        nop                      ; reset          not used
        nop                      ; undef          not used
        B       SMCCode
        nop                      ; prefetch       can be used
        nop                      ; data abort     can be used
        nop                      ; reserved
        nop                      ; irq            can be used
        nop                      ; fiq            can be used
        = "qqwweerrtt"
        ALIGN
; code run in secure mode as response to SMC #0 opcode
; runs in secure monitor mode, so can access L2 Cache controller enable bit
SMCCode
        ;MOVS        pc,lr
        MOV     r3, #HAL_WsSize ;oardConfig_Size
10      SUBS    r3, r3, #4
        LDR     r4, [r0, r3]

        STR     r3, [sb, r3]
        BGT     %BT10
        ;Pull    "r0"
        ;MSR CPSR_c,#MON32_mode+I32_bit+F32_bit
        ;MOV R1,#1+16+32 ; SCR.NS, .FW, .AW set
        ;MCR p15, 0, R1, c1, c1, 0
        ;ISB
        ;ADR R2,continue
        ;LDR  r1,=A32_bit+I32_bit+F32_bit+SVC32_mode
        ;MSR     CPSR_c, r1 ;#A32_bit+I32_bit+F32_bit+SVC32_mode
        ;STMFD R13!,{R0-R1}
        ;RFEFD R13!

        mov  r1,sb
        movs pc,lr

        Pull        "r0"
        MOV     r0,lr
        mrc	p15, 0, r5, c1, c1, 0		;@ read SCR
	bic	r5, r5, #0x4a			;@ clear IRQ, EA, nET bits
	orr	r5, r5, #0x31			;@ enable NS, AW, FW bits
						;@ FIQ preserved for secure mode
;	mov	r6, #SVC_MODE			@ default mode is SVC
;	is_cpu_virt_capable r4
;#ifdef CONFIG_ARMV7_VIRT
;;	orreq	r5, r5, #0x100			@ allow HVC instruction
;	moveq	r6, #HYP_MODE			@ Enter the kernel as HYP
;#endif

	;mcr	p15, 0, r5, c1, c1, 0		;@ write SCR (with NS bit set)
        ;orr     r5,r5,#0x1
        mcr	p15, 0, r5, c1, c1, 0
        ISB
        ;DSB     SY
        MOV      lr,r0
        movs    pc, lr           ; exit, restoring state
        ;bx       lr
; code to initialise this all and set it going
; this is set up before the MMU is on, we'll avoid using it until afterwards
;
; ****** ASSUMPTION******
;  it is assumed that the HAL starts on a megabyte boundary, and does not exceed
;  256 k in length.
;  it is also assumed that the logical HAL start address is &fc000000
;   (this address is defined in the components file, and KernelWS header file
;   in kernel)
SecureInit
        MRS     r1, CPSR          ; remember our mode
        MSR     CPSR_c, #F32_bit+I32_bit+(MON32_mode);   can we do this?
        adr     r0,SecureBase
        bic     r0,r0,#&03fc0000   ; clear any bits that are not needed
        orr     r0,r0,#&fc000000   ; bear in mind that we are at top of ram now
        MCR     p15,0,r0,c12,c0,1  ; set the Mon Mode vector base to us here
        MSR     CPSR_c, r1         ; and will be mapped to FC000000 and up
        mov     pc, lr

        END
