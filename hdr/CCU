; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2023 Robert Sprowson
; Portions Copyright 2024 John Ballance
; Use is subject to license terms.
;
;

; CCU register offsets

PLL_CPUX_CTRL_REG             * 0x0000
PLL_AUDIO_CTRL_REG            * 0x0008
PLL_VIDEO0_CTRL_REG           * 0x0010
PLL_VE_CTRL_REG               * 0x0018
PLL_DDR0_CTRL_REG             * 0x0020
PLL_PERIPH0_CTRL_REG          * 0x0028
PLL_PERIPH1_CTRL_REG          * 0x002C
PLL_VIDEO1_CTRL_REG           * 0x0030
PLL_GPU_CTRL_REG              * 0x0038
PLL_MIPI_CTRL_REG             * 0x0040
PLL_HSIC_CTRL_REG             * 0x0044
PLL_DE_CTRL_REG               * 0x0048
PLL_DDR1_CTRL_REG             * 0x004C
CPU_AXI_CFG_REG               * 0x0050
AHB1_APB1_CFG_REG             * 0x0054
APB2_CFG_REG                  * 0x0058
AHB2_CFG_REG                  * 0x005C
BUS_CLK_GATING_REG0           * 0x0060
BUS_CLK_GATING_REG1           * 0x0064
BUS_CLK_GATING_REG2           * 0x0068
AC_DIG_Clk_En                 * 1
BUS_CLK_GATING_REG3           * 0x006C
BUS_CLK_GATING_REG4           * 0x0070
THS_CLK_REG                   * 0x0074
NAND_CLK_REG                  * 0x0080
SDMMC0_CLK_REG                * 0x0088
SDMMC1_CLK_REG                * 0x008C
SDMMC2_CLK_REG                * 0x0090
TS_CLK_REG                    * 0x0098
CE_CLK_REG                    * 0x009C
SPI0_CLK_REG                  * 0x00A0
SPI1_CLK_REG                  * 0x00A4
I2S_PCM_0_CLK_REG             * 0x00B0
I2S_PCM_1_CLK_REG             * 0x00B4
I2S_PCM_2_CLK_REG             * 0x00B8
SPDIF_CLK_REG                 * 0x00C0
USBPHY_CFG_REG                * 0x00CC
DRAM_CFG_REG                  * 0x00F4
PLL_DDR_CFG_REG               * 0x00F8
MBUS_RST_REG                  * 0x00FC
DRAM_CLK_GATING_REG           * 0x0100
DE_CLK_REG                    * 0x0104
TCON0_CLK_REG                 * 0x0118
TCON1_CLK_REG                 * 0x011C
DEINTERLACE_CLK_REG           * 0x0124
CSI_MISC_CLK_REG              * 0x0130
CSI_CLK_REG                   * 0x0134
VE_CLK_REG                    * 0x013C
AC_DIG_CLK_REG                * 0x0140
X1_sclk_en                    * 0x80000000
X4_sclk_en                    * 0x40000000
AVS_CLK_REG                   * 0x0144
HDMI_CLK_REG                  * 0x0150
HDMI_SLOW_CLK_REG             * 0x0154
MBUS_CLK_REG                  * 0x015C
MIPI_DSI_CLK_REG              * 0x0168
GPU_CLK_REG                   * 0x01A0
PLL_STABLE_TIME_REG0          * 0x0200
PLL_STABLE_TIME_REG1          * 0x0204
PLL_PERIPH1_BIAS_REG          * 0x021C
PLL_CPUX_BIAS_REG             * 0x0220
PLL_AUDIO_BIAS_REG            * 0x0224
PLL_VIDEO0_BIAS_REG           * 0x0228
PLL_VE_BIAS_REG               * 0x022C
PLL_DDR0_BIAS_REG             * 0x0230
PLL_PERIPH0_BIAS_REG          * 0x0234
PLL_VIDEO1_BIAS_REG           * 0x0238
PLL_GPU_BIAS_REG              * 0x023C
PLL_MIPI_BIAS_REG             * 0x0240
PLL_HSIC_BIAS_REG             * 0x0244
PLL_DE_BIAS_REG               * 0x0248
PLL_DDR1_BIAS_REG             * 0x024C
PLL_CPUX_TUN_REG              * 0x0250
PLL_DDR0_TUN_REG              * 0x0260
PLL_MIPI_TUN_REG              * 0x0270
PLL_PERIPH1_PAT_CTRL_REG      * 0x027C
PLL_CPUX_PAT_CTRL_REG         * 0x0280
PLL_AUDIO_PAT_CTRL_REG        * 0x0284
PLL_VIDEO0_PAT_CTRL_REG       * 0x0288
PLL_VE_PAT_CTRL_REG           * 0x028C
PLL_DDR0_PAT_CTRL_REG         * 0x0290
PLL_VIDEO1_PAT_CTRL_REG       * 0x0298
PLL_GPU_PAT_CTRL_REG          * 0x029C
PLL_MIPI_PAT_CTRL_REG         * 0x02A0
PLL_HSIC_PAT_CTRL_REG         * 0x02A4
PLL_DE_PAT_CTRL_REG           * 0x02A8
PLL_DDR1_PAT_CTRL_REG0        * 0x02AC
PLL_DDR1_PAT_CTRL_REG1        * 0x02B0
BUS_SOFT_RST_REG0             * 0x02C0
BUS_SOFT_RST_REG1             * 0x02C4
BUS_SOFT_RST_REG2             * 0x02C8
BUS_SOFT_RST_REG3             * 0x02D0
AC_Rst                        * 1
BUS_SOFT_RST_REG4             * 0x02D8
CCM_SEC_SWITCH_REG            * 0x02F0
PS_CTRL_REG                   * 0x0300
PS_CNT_REG                    * 0x0304
PLL_LOCK_CTRL_REG             * 0x0320

        END
