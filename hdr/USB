; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
        [       :LNOT: :DEF: __HAL_USB_HDR__
        GBLL    __HAL_USB_HDR__

; OTG Phy control registers
USB_Phy_BASE        *         0x400           ; base offset of these registers
                                              ; from OTG USB_Device base
USB_Phy_ISCR        *         0x00
USB_Phy_PHYCTL      *         0x04
USB_Phy_PHYBIST     *         0x08
USB_Phy_PHYTUNE     *         0x0c
USB_Phy_Vend0       *         0x10
USB_Phy_EhHostEn    *         0x20

; Register bits in  USB_Phy_ISCR
USB_Phy_VBFD        *         (1<<30)
USB_Phy_VBFV        *         (1<<29)
USB_Phy_EXTID       *         (1<<28)
USB_Phy_EXTDM       *         (1<<27)
USB_Phy_EXTDP       *         (1<<26)
USB_Phy_MVBS        *         (1<<25)
USB_Phy_MIDS        *         (1<<24)
USB_Phy_IDPEN       *         (1<<17)
USB_Phy_DPMPEN      *         (1<<16)
USB_Phy_FIDOFF      *         (0<<14)
USB_Phy_FIDL        *         (2<<14)
USB_Phy_FIDH        *         (3<<14)
USB_Phy_VBVOFF      *         (0<<12)
USB_Phy_VBVL        *         (2<<12)
USB_Phy_VBVH        *         (3<<12)
USB_Phy_HOSC_EN     *         (1<<7)
        ; irq ish bits write 1 to clear
USB_Phy_VBCHG       *         (1<<6)
USB_Phy_IDCHG       *         (1<<5)
USB_Phy_DMPCHG      *         (1<<4)
USB_Phy_IRQonWake   *         (1<<3)
USB_Phy_IRQonVBCHG  *         (1<<2)
USB_Phy_IRQonIDCHG  *         (1<<1)
USB_Phy_IRQonDMPCHG *         (1<<0)

; Fields in USB_Phy_PHYCTL register
PHYCTL_DataR         *         (1<<16)
PHYCTL_Addr_shift    *         (8)
PHYCTL_Addr          *         (255<<PHYCTL_Addr_shift)
PHYCTL_DataW         *         (1<<7)
PHYCTL_Strb          *         (1<<0)
; PHYCTL addresses
PHYCTL_PLLBW         *         0x03
PHYCTL_PLLBW_mask    *         (3<<0)
PHYCTL_R45_CALEN     *         0x0c
PHYCTL_R45_CALEN_mask *        (1<<0)
PHYCTL_TX_AMP        *         0x20
PHYCTL_TX_AMP_mask   *         (3<<0)
PHYCTL_TX_SLEW       *         0x22
PHYCTL_TX_SLEW_mask  *         (7<<0)
PHYCTL_VB_VALID      *         0x25
PHYCTL_VB_VALID_mask *         (3<<0)
PHYCTL_OTG_EN        *         0x28
PHYCTL_OTG_EN_mask   *         (1<<0)
PHYCTL_VB_DETEN      *         0x29
PHYCTL_VB_DETEN_mask *         (1<<0)
PHYCTL_DISCON_DETEN  *         0x2a
PHYCTL_DISCON_DETEN_mask *     (7<<0)

; macro writes a predetermined hardcoded bit..
; Assumes sb initialised
        MACRO
        PHYCTLWrite $reg,$bit
        PUSH       "a1,a2"
        mov        a1, #(($reg << PHYCTL_Addr_shift) & PHYCTL_Addr) +($bit & $bit_mask) + PHYCTL_Strb
        ldr        a2, USB_Device_log




        PULL       "a1,a2"
        MEND

        ]
        END
