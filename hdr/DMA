; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;

; DMA registers - relative to A64_DMA
DMA_IRQ_EN_REG                * &00
DMA_IRQ_PEND_REG              * &10
DMA_SEC_REG                   * &20
DMA_AUTO_GATE_REG             * &28
DMA_STA_REG                   * &30
; Per-channel registers
DMA_CH_EN_REG                 * &100
DMA_CH_PAU_REG                * &104
DMA_CH_DESC_ADDR_REG          * &108
DMA_CH_CFG_REG                * &10C
DMA_CH_CUR_SRC_REG            * &110
DMA_CH_CUR_DEST_REG           * &114
DMA_CH_BCNT_LEFT_REG          * &118
DMA_CH_PARA_REG               * &11C
DMA_CH_MODE_REG               * &128
DMA_CH_FDESC_ADDR_REG         * &12C ; Address of current descriptor
DMA_CH_PKG_NUM_REG            * &130

DMA_CH_REG_shift              * 6 ; 64 byte offset between each channel register block
DMA_CH_count                  * 7 ; 7 channels usable by DMAManager, 8th channel used for GraphicsV

; DMA descriptor
                 ^ 0
DMA_DESC_CFG     # 4 ; DMA_CH_CFG_REG
DMA_DESC_SRC     # 4 ; DMA_CH_CUR_SRC_REG
DMA_DESC_DEST    # 4 ; DMA_CH_CUR_DEST_REG
DMA_DESC_BCNT    # 4 ; DMA_CH_BCNT_LEFT_REG
DMA_DESC_PARA    # 4 ; DMA_CH_PARA_REG
DMA_DESC_LINK    # 4 ; DMA_CH_DESC_ADDR_REG
DMA_DESC_total   # 4 ; Extra parameter we use to store the total byte count that'll have been transferred at the start of this descriptor
DMA_DESC_size    # 0

DMA_DESC_terminator * &FFFFF800 ; Address value that terminates descriptor chain

; Peripheral DRQ values
DMA_DRQ_SRAM     * 0
DMA_DRQ_SDRAM    * 1
DMA_DRQ_OWA      * 2 ; Only valid as dest
DMA_DRQ_PCM0     * 3
DMA_DRQ_PCM1     * 4
DMA_DRQ_NAND     * 5
DMA_DRQ_UART0    * 6
DMA_DRQ_UART1    * 7
DMA_DRQ_UART2    * 8
DMA_DRQ_UART3    * 9
DMA_DRQ_UART4    * 10
DMA_DRQ_AUDIO    * 15
DMA_DRQ_USB_EP1  * 17
DMA_DRQ_USB_EP2  * 18
DMA_DRQ_USB_EP3  * 19
DMA_DRQ_USB_EP4  * 20
DMA_DRQ_USB_EP5  * 21
DMA_DRQ_SPI0     * 23
DMA_DRQ_SPI1     * 24
DMA_DRQ_PCM2     * 27 ; Only valid as dest

        END
